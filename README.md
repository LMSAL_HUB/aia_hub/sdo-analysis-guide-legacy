# SDO Data Analysis Guide (Legacy)

## What is this Repo?

This repo contains the source files for the legacy SDO Data Analysis Guide. A
single LaTeX source file produces both the PDF and HTML versions of the
Guide. The output HTML is then uploaded to the SDODocs team area and served
from <https://www.lmsal.com/sdouserguide.html>.

## Source Files

To streamline the workflow, there is a single LaTeX source for the SDO
Data Analysis Guide that generates both the PDF and the HTML
versions. This file is `src-latex/sdoguide.tex`.

The PDF can be generated directly from the `sdoguide.tex` file in this
repo by using a command like

`pdflatex sdoguide.tex`

An add-on package named `tex4ht` is used to generate an indexed set of
HTML files from `sdoguide.tex`, which can be created by a command like

`htlatex sdoguide "sdoguide,2,frames" "" "-d/Users/yourname/public_html/sdoguide/ -p"`

Basic information about the `tex4ht` package is found at
<http://tug.org/applications/tex4ht>. The `htlatex` command above generates a
set of hierarchical webpages (one for each subsection) that can be navigated
from a sidebar. It uses frames. The look and feel of the webpages is
determined by the style-sheet specifications in `sdoguide.cfg`.

Images used for the figures in the Guide are mostly PNG files taken from
screenshots, and these are kept in the `figures` directory. In
`src-latex/sdoguide.cfg` there is some code (demonstrated at
<http://www.tug.org/applications/tex4ht/mn5.html#QQ1-5-28>) that creates
clickable thumbnail images in the HTML version of the Guide.

## Workflow

The workflow used to process the LaTeX source files and upload them to the
SDODocs team area is as follows:

1.  Create a destination directory to contain both the PDF and the set of
static HTML webpages, for example
`/Users/yourname/public_html/sdoguide/`. (Using a `public_html` area allows
previewing the HTML before uploading them to SDOdocs, but this is not strictly
necessary.)

1.  Make a `figures` directory in the destination directory, and copy all of
the PNGs there by doing something like

	`cp -rp figures /Users/yourname/public_html/sdoguide/figures/`

2.  Create the PDF version by doing `pdflatex sdoguide.tex`, and repeating
enough times so that it compiles without errors or warnings.

3.  Copy this PDF file to the destination directory by doing a

	`cp -p sdoguide.pdf /Users/yourname/public_html/sdoguide/`

4.  Create HTML version by using the `htlatex` command.

6.  Navigate to `/Users/yourname/public_html/sdoguide/` and run the
`script_postprocess1.csh` script.

6.  Then, while in `/Users/yourname/public_html/sdoguide/`, link the top-level
HTML file to `index.html` via

	`ln -s sdoguide.html index.html`

7.  Then, while still in `/Users/yourname/public_html/sdoguide/`, make the ZIP
    archive by doing a

	`zip -r sdoguide.zip figures index.html sdoguide*`

8.  Go to the SDOdocs team area, log in, create a new revision, and
then upload both the ZIP file and the PDF file from the
`/Users/yourname/public_html/sdoguide` area.

9.  The online version of the guide should now be updated. If you are unsure
(or paranoid), you can double-check on SDOdocs to make sure the links takes
users to the HTML version, and then check the PDF link on the
table-of-contents sidebar.
